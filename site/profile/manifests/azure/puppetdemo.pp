# Temporary Profile
class profile::azure::puppetdemo {

  azure_vm { 'azure-pe-testvm':
    ensure                        => absent,
    location                      => 'australiaeast',
    image                         => 'RedHat:RHEL:7.2:latest',
    user                          => 'pickles',
    password                      => 'Lolol0lolol!123',
    size                          => 'Standard_F1s',
    resource_group                => 'AzureDemo',
    dns_domain_name               => '3ahpaas1mdmefclebrrdeoy4bh.px.internal.cloudapp.net',
    dns_servers                   => '168.63.129.16',
    virtual_network_name          => 'AzureDemo-vnet',
    virtual_network_address_space => '10.0.0.0/24',
    subnet_name                   => 'default',
    subnet_address_prefix         => '10.0.0.0/24',
    private_ip_allocation_method  => 'Dynamic',
    public_ip_allocation_method   => 'None',
    # extensions                    => {
    #   'CustomScriptForLinux' => {
    #     'auto_upgrade_minor_version' => false,
    #     'publisher'                  => 'Microsoft.OSTCExtensions',
    #     'type'                       => 'CustomScriptForLinux',
    #     'type_handler_version'       => '1.4',
    #     'settings'                   => {
    #       'commandToExecute' => 'echo "10.0.0.10 puppet.local" >> /etc/hosts; curl -k https://puppet.local:8140/packages/current/install.bash | sudo bash -s agent:certname=azure-pe-testvm.local',
    #     },
    #   },
    # },
  }

  azure_vm { 'proxypuppet':
    ensure                     => absent,
    image                      => 'RedHat:RHEL:7.2:latest',
    location                   => 'australiaeast',
    network_interface_name     => 'proxypuppet794',
    os_disk_caching            => 'ReadWrite',
    os_disk_create_option      => 'FromImage',
    os_disk_name               => 'proxypuppet',
    os_disk_vhd_container_name => 'vhds',
    os_disk_vhd_name           => 'proxypuppet20170407121441',
    resource_group             => 'azuredemo',
    size                       => 'Standard_F1s',
    user                       => 'picker0t',
  }

  azure_vm { 'puppet':
    ensure                 => absent,
    image                  => 'RedHat:RHEL:7.2:latest',
    location               => 'australiaeast',
    network_interface_name => 'puppet39',
    os_disk_caching        => 'ReadWrite',
    os_disk_create_option  => 'FromImage',
    os_disk_name           => 'puppet',
    resource_group         => 'azuredemo',
    size                   => 'Standard_DS2_v2_Promo',
    user                   => 'picker0t',
  }

}
