# Temporary Profile
class profile::azure::demo {

  azure_vm { 'azure-demo':
    ensure                        => absent,
    location                      => 'australiaeast',
    image                         => 'RedHat:RHEL:7.2:latest',
    user                          => 'pickles',
    password                      => 'Lolol0lolol!123',
    size                          => 'Standard_F1s',
    resource_group                => 'AzureDemo',
    dns_domain_name               => '3ahpaas1mdmefclebrrdeoy4bh.px.internal.cloudapp.net',
    dns_servers                   => '168.63.129.16',
    virtual_network_name          => 'AzureDemo-vnet',
    virtual_network_address_space => '10.0.0.0/24',
    subnet_name                   => 'default',
    subnet_address_prefix         => '10.0.0.0/24',
    private_ip_allocation_method  => 'Dynamic',
    public_ip_allocation_method   => 'None',
    extensions                    => {
      'CustomScriptForLinux' => {
        'auto_upgrade_minor_version' =>  false,
        'publisher'                  =>  'Microsoft.OSTCExtensions',
        'type'                       =>  'CustomScriptForLinux',
        'type_handler_version'       =>  '1.4',
        'settings'                   =>  {
          'commandToExecute' =>  'bash puppet.sh',
          'fileUris'         =>  ['https://pcklscripts.blob.core.windows.net/puppet/puppet.sh'],
        },
      },
    },
  }

}
