# Profile to configure a node that talks to AWS, Azure etc...
class profile::cloudproxy (
  $azure_config = '',
) {

  # AWS Stuff

  # /opt/puppetlabs/puppet/bin/gem install aws-sdk-core retries --no-ri --no-rdoc

  # ~/.aws/credentials
  # /etc/puppetlabs/puppet/puppetlabs_aws_configuration.ini


  # Azure Stuff

  # curl --silent --location https://rpm.nodesource.com/setup_7.x | bash -

  $packages = [
    'gcc',
    'git',
    'libffi-devel',
    'nodejs',
    'openssl-devel',
    'python-devel',
    'ruby-devel',
  ]

  ensure_packages($packages)

  # yum groupinstall 'Development Tools'

  # npm install -g azure-cli
  #
  # /opt/puppetlabs/puppet/bin/gem install retries --no-ri --no-rdoc
  # /opt/puppetlabs/puppet/bin/gem install azure --version='~>0.7.0' --no-ri --no-rdoc
  # /opt/puppetlabs/puppet/bin/gem install azure_mgmt_compute --version='~>0.3.0' --no-ri --no-rdoc
  # /opt/puppetlabs/puppet/bin/gem install azure_mgmt_storage --version='~>0.3.0' --no-ri --no-rdoc
  # /opt/puppetlabs/puppet/bin/gem install azure_mgmt_resources --version='~>0.3.0' --no-ri --no-rdoc
  # /opt/puppetlabs/puppet/bin/gem install azure_mgmt_network --version='~>0.3.0' --no-ri --no-rdoc
  # /opt/puppetlabs/puppet/bin/gem install hocon --version='~>1.1.2' --no-ri --no-rdoc

  # Generated from `puppet resource file /etc/puppetlabs/puppet/azure.conf`
  file { '/etc/puppetlabs/puppet/azure.conf':
    ensure  => 'file',
    content => $azure_config,
    mode    => '0644',
  }

}
