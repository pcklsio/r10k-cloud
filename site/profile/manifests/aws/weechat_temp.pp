# Profile for AWS ec2_instance
class profile::aws::weechat_temp {

  ec2_instance { 'weechat-temp':
    ensure            => 'stopped',
    availability_zone => 'ap-southeast-2c',
    image_id          => 'ami-8bf2fde8',
    instance_type     => 't2.nano',
    key_name          => 'weechat-temp',
    region            => 'ap-southeast-2',
  }

}
