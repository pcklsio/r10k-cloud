# Profile for AWS ec2_instance
class profile::aws::demo {

  ec2_instance { 'aws-demo':
    ensure            => absent,
    availability_zone => 'ap-southeast-2a',
    image_id          => 'ami-4e686b2d',
    instance_type     => 't2.nano',
    key_name          => 'weechat-temp',
    region            => 'ap-southeast-2',
    security_groups   => ['default'],
    subnet            => 'default-subnet-ap-southeast-2a',
    user_data         => template('profile/puppet.sh.erb'),
  }

}
